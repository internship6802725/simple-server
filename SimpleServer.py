from http.server import BaseHTTPRequestHandler, HTTPServer
import json

LOG_FILE = 'requests.ndjson'

def root(request=None):
        return { 'code': 200, 'body': 'Index Page', 'headers': {'Content-Type': 'text/html'} }

def helloworld(request=None):
    return { 'code': 200, 'body': 'Hello, World!', 'headers': {'Content-Type': 'text/html'} }

def ping(request=None):
    return { 'code': 200, 'body': 'pong', 'headers': {'Content-Type': 'text/html'} }

def notfound(request=None):
    return { 'code': 404, 'body': 'Not Found', 'headers': {'Content-Type': 'text/html'} }

paths = {
    '/': root,
    '/helloworld': helloworld,
    '/ping': ping,
}

def lookup(request):
    path = request['url']
    return paths[path] if path in paths else notfound


class ReqHandler(BaseHTTPRequestHandler):


    def compose_request(self):
        url = self.path
        headers = self.headers

        return { 'url': url, 'headers': headers }


    def do_GET(self):

        request = self.compose_request()
        handler = lookup(request)
        response = handler(request)

        self.send_response(response['code'])
        self.send_header('Content-type', response['headers']['Content-Type'])
        self.end_headers()
        self.wfile.write(response['body'].encode())

        self.log_req(self.path, response)
        

    def log_req(self, path, data):
        log = {
            'method': 'GET',
            'path': path,
            'data': data,
        }
        with open(LOG_FILE, 'a') as f:
            json.dump(log, f)
            f.write('\n') 

    


def run(server_class=HTTPServer, handler_class=ReqHandler, port=8000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print(f'Starting httpd server on port {port}')
    httpd.serve_forever()

if __name__ == '__main__':
    run()

