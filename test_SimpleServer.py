import unittest
from SimpleServer import root, helloworld, ping, notfound

class TestSimpleHTTPRequestHandler(unittest.TestCase):

    def test_root(self):
        response = root()
        self.assertEqual(response['code'], 200)
        self.assertEqual(response['body'], 'Index Page')

    def test_helloworld(self):
        response = helloworld()
        self.assertEqual(response['code'], 200)
        self.assertEqual(response['body'], 'Hello, World!')

    def test_ping(self):
        response = ping()
        self.assertEqual(response['code'], 200)
        self.assertEqual(response['body'], 'pong')

    def test_notfound(self):
        response = notfound()
        self.assertEqual(response['code'], 404)
        self.assertEqual(response['body'], 'Not Found')


if __name__ == '__main__':
    unittest.main()