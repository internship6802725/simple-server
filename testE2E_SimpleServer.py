import unittest
import requests
import multiprocessing
import time
from SimpleServer import run

class TestE2E(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.server_process = multiprocessing.Process(target=run)
        cls.server_process.start()
        time.sleep(1)  # Give the server a second to start

    @classmethod
    def tearDownClass(cls):
        cls.server_process.terminate()
        cls.server_process.join()

    def test_root(self):
        response = requests.get('http://localhost:8000')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.text, 'Index Page')

    def test_helloworld(self):
        response = requests.get('http://localhost:8000/helloworld')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.text, 'Hello, World!')

    def test_ping(self):
        response = requests.get('http://localhost:8000/ping')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.text, 'pong')

    def test_not_found(self):
        response = requests.get('http://localhost:8000/notfound')
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.text, 'Not Found')

if __name__ == '__main__':
    unittest.main()